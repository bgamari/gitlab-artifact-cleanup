let nixpkgs = import <nixpkgs> {};
in with nixpkgs;
let
  python-gitlab = python3Packages.buildPythonPackage rec {
    pname = "python-gitlab";
    version = "1.7.0";
    src = fetchFromGitHub {
      owner = "python-gitlab";
      repo = "python-gitlab";
      rev = version;
      sha256 = "0ya2y26pgz41c2l7b6xfdnpj3z8ph0y3g0q05g3xb0z6c6mgklks";
    };
    propagatedBuildInputs = with python3Packages; [ requests six ];
    doCheck = false;
  };

in stdenv.mkDerivation {
  name = "gitlab-artifact-cleanup";
  src = ./.;
  propagatedBuildInputs = with python3Packages; [ dateutil pytz python-gitlab ];
  buildPhase = "";
}
